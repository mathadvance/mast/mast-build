# MAST Build

This is the build script for MAST units. To build you `cd` into the unit directory. It must contain a file called `unit.tex`, and files `problem-1.tex` through `problem-n.tex`.

Run `mast-build n` to build version `n` of the unit, and run `mast-build` to build every version of the unit.

## Install
Make sure you have Rust [installed](rust-lang.org/tools/install) and run
    cargo install mast-build

## Requirements

You must have a working version of LaTeX (preferably TeX Live) and `latexmk`. If you have a custom `latexmk` script it must compile a PDF with the same name as the `.tex` file and must place it in the same directory as the `.tex` file.

## Internal Documentation

If you have a copy of `mast-build` pulled and want to look at the automatically generated documentation, run `cargo doc --no-deps --open`.
