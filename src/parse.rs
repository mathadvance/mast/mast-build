/// Extracts version number from problem-set file path
///
/// # Usage
///
/// ```
/// let version = path_str_to_version("./problems-1.tex");
/// assert_eq!("1", version);
/// let text_version = path_str_to_version("./problems-text.tex");
/// assert_eq!("text", text_version);
/// ```

pub fn version(path_str: &str) -> &str {
    if path_str.len() < 15 {
        return "";
    }
    let first = &path_str[2..11];
    let last = &path_str[path_str.len() - 4..];
    if first == "problems-" && last == ".tex" {
        return &path_str[11..path_str.len() - 4];
    } else {
        return "";
    }
}

#[cfg(test)]
mod version_tests {
    use super::version;
    fn test(path: &str, result: &str) {
        assert_eq!(version(path), result);
    }
    #[test]
    fn batch_test() {
        test("./problems-1.tex", "1");
        test("./problems-text.tex", "text");
    }
}

/// Extracts metadata (name and author) from yaml
///
/// # Usage
///
/// ```
/// let yaml = "name: Perspectives\nauthor: Dennis Chen";
/// assert_eq([String::from("Perspectives"), String::from("Dennis Chen")], metadata(yaml));
/// ```

pub fn metadata(yaml: &str) -> [String; 2] {
    const EMPTY_STRING: String = String::new();
    let mut metadata: [String; 2] = [EMPTY_STRING; 2]; // https://stackoverflow.com/a/70267984
    let parsed_yaml = strict_yaml_rust::StrictYamlLoader::load_from_str(yaml);
    match parsed_yaml {
        Ok(config) => {
            let name = &config[0]["name"].as_str();
            match name {
                Some(value) => {
                    metadata[0] = value.to_string();
                }
                None => {
                    println!("Error: Could not parse value associated with `author' key in `metadata.yml'");
                    println!("Ensure that the value associated with the `author' key exists and is a string.");
                    quit::with_code(exitcode::DATAERR);
                }
            }
            let author = &config[0]["author"].as_str();
            match author {
                Some(value) => {
                    metadata[1] = value.to_string();
                }
                None => {
                    println!("Error: Could not parse value associated with `author' key in `metadata.yml'");
                    println!("Ensure that the value associated with the `author' key exists and is a string.");
                    quit::with_code(exitcode::DATAERR);
                }
            }
        }
        Err(_) => {
            println!("Error: Could not parse `metadata.yml'");
            quit::with_code(exitcode::DATAERR);
        }
    };
    return metadata;
}

#[cfg(test)]
mod metadata_tests {
    use super::metadata;
    fn test(yaml: &str, result: [String; 2]) {
        assert_eq!(metadata(yaml), result);
    }
    #[test]
    fn batch_test() {
        test(
            "name: Perspectives\nauthor: Dennis Chen",
            [String::from("Perspectives"), String::from("Dennis Chen")],
        );
        test(
            "name: Perspectives\nauthor:",
            [String::from("Perspectives"), String::from("")],
        )
    }
    #[test]
    #[should_panic]
    fn fail() {
        test(
            "name: Perspectives\n",
            [String::from("Perspectives"), String::from("")],
        );
    }
}
