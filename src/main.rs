//! # mast-build
//!
//! `mast-build` is a binary script that compiles handouts with the [mast](https://gitlab.com/mathadvance/tex/mast) LaTeX class.
//! It was written to make compiling handouts with multiple versions of problem-sets and their corresponding solutions easier.
//!
//! ## Usage
//!
//! To build you `cd` into the unit directory. It must contain a file called `unit.tex`, and files `problem-1.tex` through `problem-n.tex`.
//!
//! To run, type `mast-build n` to build version `n` of the unit, and run `mast-build` to build every version of the unit.
//!
//! ## Requirements
//!
//! You must have a working version of LaTeX (preferably TeX Live) and `latexmk`. If you have a custom `latexmk` script it must compile a PDF with the same name as the `.tex` file and must place it in the same directory as the `.tex` file.

use std::env;
use std::fs;
use std::path::Path;
use std::process::Command;

mod parse;

/// Compiles a unit given a unit code, metadata, and a version
///
/// # Usage
///
///```
/// let unit_code = "CQV-Perspectives";
/// let mut metadata: [String; 2] = [EMPTY_STRING; 2];
/// metadata[0] = "Perspectives";
/// metadata[1] = "Dennis Chen";
/// let version = "1";
/// compile(unit_code, &metadata, version);
///```

pub fn compile(unit_code: &str, metadata: &[String; 2], version: &str) {
    let build_str = ["build-", version].concat();
    let build_path = Path::new(&build_str);
    fs::create_dir(&build_path).ok();

    let unit_tex_path = [unit_code, "-", version, ".tex"].concat();
    fs::copy(Path::new("unit.tex"), build_path.join(&unit_tex_path))
        .expect(&["Error: Could not copy `unit.tex' to ", &build_str].concat());
    let solutions_tex_path = ["S-", unit_code, "-", version, ".tex"].concat();
    fs::write(
        build_path.join(&solutions_tex_path),
        [
            "\\documentclass{mast}\n",
            "\\source{",
            &[unit_code, version].join("-"),
            "}\n",
            "\\begin{document}\n",
            "\\maketitle\n",
            "\\toc\n",
            "\\solprint\n",
            "\\end{document}\n",
        ]
        .concat(),
    )
    .ok();

    let problem_tex_path = ["problems-", version, ".tex"].concat();
    fs::copy(
        Path::new(&problem_tex_path),
        build_path.join("problems.tex"),
    )
    .expect(
        &[
            "Error: Could not copy `",
            &problem_tex_path,
            "' to ",
            &build_str,
        ]
        .concat(),
    );

    let unit_pdf_path = [unit_code, "-", version, ".pdf"].concat();
    let solutions_pdf_path = ["S-", unit_code, "-", version, ".pdf"].concat();

    if Path::new(&unit_pdf_path).exists() {
        fs::rename(&unit_pdf_path, build_path.join(&unit_pdf_path)).ok();
    }

    env::set_current_dir(&build_path).ok();

    fs::write(
        [unit_code, version, "metadata.tex"].join("-"),
        [
            "\\title{",
            &metadata[0].to_owned(),
            "}\n",
            "\\author{",
            &metadata[1].to_owned(),
            "}\n",
            "\\date{",
            &[&unit_code[..3], version].join("-"),
            "}\n",
        ]
        .concat(),
    )
    .expect(&["Error copying metadata.tex to ", &build_str].concat());

    let unit_latexmk = Command::new("latexmk")
        .args(["-pdf", "-f", &unit_tex_path])
        .output()
        .expect("Failed to execute latexmk");

    if unit_latexmk.status.success() {
        println!("{}", &["Finished compiling ", &unit_pdf_path].concat());
    } else {
        println!("{}", &["Warning: Failed to compile ", &unit_pdf_path].concat());
    }

    fs::write(
        ["S", unit_code, version, "metadata.tex"].join("-"),
        [
            "\\title{Solutions to ",
            &metadata[0].to_owned(),
            "}\n",
            "\\author{",
            &metadata[1].to_owned(),
            "}\n",
            "\\date{",
            &["S", &unit_code[..3], version].join("-"),
            "}\n",
        ]
        .concat(),
    )
    .expect(&["Error copying metadata.tex to ", &build_str].concat());

    let solutions_latexmk = Command::new("latexmk")
        .args(["-pdf", "-f", &solutions_tex_path])
        .output()
        .expect("Failed to execute latexmk");

    if solutions_latexmk.status.success() {
        println!("{}", &["Finished compiling ", &solutions_pdf_path].concat());
    } else {
        println!("{}", &["Warning: Failed to compile ", &solutions_pdf_path].concat());
    }

    env::set_current_dir("../").ok();

    fs::rename(build_path.join(&unit_pdf_path), &unit_pdf_path).ok();
    fs::rename(build_path.join(&solutions_pdf_path), &solutions_pdf_path).ok();
}

/// Handles preliminary setup, such as checking if a help flag was passed, validity checks, and metadata parsing. Also calls the compile function.

#[quit::main]
pub fn main() {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0); // Remove the first argument which is "mast-build" itself

    if args.iter().any(|arg| arg == "--help") {
        println!("Usage: mast-build [VERSIONS]...");
        println!("Builds MAST units.");
        println!("If no versions are passed in, all versions will be compiled.");
        quit::with_code(exitcode::OK);
    }

    // Check validity directories and files so we can handle error immediately
    let cwd = env::current_dir().unwrap();
    let dir_name = cwd.file_name().unwrap();
    let unit_code: &str = dir_name.to_str().unwrap();

    if unit_code.len() < 3 {
        eprintln!("Error: Directory name not long enough");
        quit::with_code(exitcode::DATAERR);
    }

    if !Path::new("unit.tex").exists() {
        eprintln!("Error: No file `unit.tex'");
        eprintln!("Make sure the TeX document containing the unit content is called `unit.tex'.");
        quit::with_code(exitcode::NOINPUT);
    }

    const EMPTY_STRING: String = String::new(); // https://stackoverflow.com/a/70267984
    let mut metadata: [String; 2] = [EMPTY_STRING; 2];

    if Path::new("metadata.yml").exists() {
        let yaml = fs::read_to_string("metadata.yml")
            .expect("Something went wrong reading `metadata.yml'");
        metadata = parse::metadata(&yaml);
    } else {
        metadata[0] = String::from(unit_code);
        metadata[1] = String::from("MAST");
        println!("Warning: No file `metadata.yml', automatically setting unit name and author");
        println!("Consider creating the file `metadata.yml' and setting the unit name, author, and description.")
    }

    if args.is_empty() {
        let files = fs::read_dir(".").unwrap();
        for element in files {
            let path = element.unwrap().path();
            let path_str = path.into_os_string().into_string().unwrap();
            let version = parse::version(&path_str);
            if version != "" {
                compile(unit_code, &metadata, version);
            }
        }
    } else {
        for arg in args {
            compile(unit_code, &metadata, &arg);
        }
    }
}
